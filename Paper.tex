\documentclass[sigconf,review, anonymous]{acmart}
\acmConference[ISSTA 2018]{ACM SIGSOFT International Symposium on Software Testing and Analysis}{16–22 July, 2018}{Amsterdam, The Netherlands}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{array}
\usepackage{booktabs}
\usepackage{multicol}
\usepackage{lipsum}
\graphicspath{ {images/} }

\begin{document}
\title{Software Defect Prediction Using Deep Neural Networks}
\maketitle


\section{Abstract}
Many software projects are shipped to customers containing defects. To avoid this, many software companies allocate testing and quality assurance budget. Increasing size of software poses challenges to traditional testing approaches due to needing for scalability. Defect prediction models have been used to direct testing efforts to probable causes of defects in the software.
Early approaches for software fault prediction relied on statistical approaches to classify software modules and decide whether each module is a fault-prone module or not. lately, many researchers tried to use machine learning techniques to train a model that can classify software modules to fault-prone modules and not fault-prone modules. Starting from the new millennium neural networks and deep learning won many competitions in machine learning applications. However, it was not investigated thoroughly to build a software defect prediction model. In this paper, we used a deep neural network to build a software defect prediction model and compared our proposed model with other machine learning algorithms like random forests, decision trees, and naive Bayes. The result shows the superiority of our proposed model over the other learning models in most of the comparisons. 
\section{Introduction}
Software testing is a crucial task in software development lifecycle to ensure software quality. The bigger the software system is, the more time and effort the testing process will take. A survey indicated that in 2016 31\% of the total IT cost was spent on QA (quality assurance) and testing covering all aspects of QA and testing including processes, tools and resource costs\cite{survey}. The study also predicts that this percentage will increase to 40\% in 2019. According to International Software Testing Qualification Board (ISTQB), The quality of a software is inversely proportional to the delivered defect and the density of defects. Unfortunately, there's no 100\% defect free software exiting due to the infeasibility of exhaustive or 100\% testing. Hence there's a need for smart tools to predict the defective modules in a software which may require more attention in testing. 
\\
Software defect prediction can help to reduce QA cost and improve the quality of the software. Software defect prediction is the process to predict the defective modules in early phases of software development lifecycle. Machine learning techniques can serve this purpose. This can be done by building a model to classify software modules as defect prone or not. The model will be developed in the learning phase, in this phase, the previous versions of the target software are fed to the model and the model will learn how to classify between the defect-prone modules and non-defect prone modules. Some metrics are extracted from each module in the target software and these metrics are fed as inputs to the model. Also, each software module has a label which indicates whether this module is defective or not. This label will be used by the model as the output and the model will try to find a relation between the input (the metrics) and the output label (defective label). After that, the model will be used to classify the defect-prone modules from the non-defect prone modules in the current version of the target software. The identified defect prone modules are prioritized higher in the testing phase. The classification task can be done by various methods. Most popular methods which are used in the same context are decision trees, random forests, naive Bayesian, Neural Networks and support vector machines.
\\
Our research objective in this work is to compare different popular machine learning algorithms and compare their performance against the deep neural network. Specifically, we compared the performance of deep neural network against the random forest, decision trees, and naive Bayes. To properly run the experiment the same datasets were fed to all techniques to raise the validity of the results. To reach that goal we built a software defect prediction model using a deep neural network and conducted an experiment to compare different techniques. Our main research question is how deep neural network classifier performs against the other classifiers in predicting faulty modules.
\\
The rest of this paper is structured as follows. Section 3 introduces the related work in this area and gives a brief introduction about deep neural networks and how we used it to build our model. Section 4 describes the experimental setup and its results. Section 5 discusses the threats to validity. The last section contains the conclusion and feature work
\section{Related Work and Background}
Various machine learning models were studied to build a defect prediction model. In this section we will discuss some of the previous work in developing a defect prediction models then we will describe the background of deep learning and how we use deep learning to develop our proposed model
\subsection{Related Work}
Many papers on building a software defect prediction have been proposed in the literature. the primary studies use software metrics as independent variables for measuring the quality of software modules. McCabe metrics \cite{mccabe} were an attempt to characterize code feature that is related to software quality. McCabe measures each module (the smallest unit of code) individually. A module in McCabe metrics is considered as functions or methods. The McCabe metrics are a collection of four software metrics. They are cyclomatic complexity, essential complexity, design complexity and lines of code (LOC). McCabe argued that code with complicated pathways are more error-prone. So McCabe module reflects the pathways within a code module.
\\
Halstead metrics \cite{hals} are similar to McCabe metrics in terms of the unit of code. The Halstead metrics fall into three groups. Base measures, derived measures and line of code measures. Halstead argued that code that's hard to read is more likely to be fault-prone.
Halstead estimates reading complexity by counting the number of concepts in a module like a number of unique operators. Many researchers \cite{pm1,pm2,pm3,pm4} used McCabe and Halstead metrics to build a software fault prediction model. However other researchers \cite{pm5,pm6} argued that derived Halstead metrics do not contain any extra information for software fault prediction as the four base metrics of Halstead describe the variation of all of the rest of Halstead metrics.
There are also Childamber and Kemerer metrics which are used to analyze only object-oriented programming paradigms. The CK metrics \cite{oom1} aim at measure software metrics related to OO principles. So the CK metrics can be grouped into 3 categories.
\\
\begin{itemize}
  \item Identification of classes. \\This category focuses on 3 features and they are Weighted methods for class, Depth for inheritance tree and number of children.
  \item Semantics of classes. \\This category focuses on 3 features and they are weighted methods for class, response for class and lack of cohesion of methods
  \item Relationship between classes. \\This category focuses on 2 features and they are response for class and coupling between objects
\end{itemize}
These features are briefly discussed in \cite{oom2}. Many researchers have used the CK metrics to build a software fault prediction model \cite{oom3,oom4,oom5}. Also, it has been proven in \cite{oom6} that there's a relation between object-oriented metrics and some external quality attributes like reliability, maintainability, effectiveness, and functionality.
As it's shown in \cite{gen1} that object-oriented metrics are useful for build an efficient software fault prediction model. In our study, we will focus more on CK metrics and we will select some features from McCabe and Halstead metrics according to feature selection algorithms like PCA \cite{pm6}. Now in the next section, we are going to briefly discuss the learning based methods used to build a predictive model.
By reviewing the research has been done to build a software fault prediction model we found that many machine learning ways are used to build this model. It's shown in \cite{gen1} that machine learning techniques perform better than statistical methods and there is a full list of the most common machine learning techniques used to build a defect prediction model. In \cite{dt1} decision trees have been used to build a prediction model using software metrics generated from exception handling calls graph. in this study, the model is validated through a case study of Hadoop Core using data mined from software repositories and defect reports. In \cite{oom4} six well-known classification algorithms have been used to build a predictive model and it's shown that naive Bayes is a robust machine learning algorithm for supervised software defect prediction problems. In \cite{pm5} A combination of traditional Artificial Neural Network (ANN) and the novel Artificial Bee Colony (ABC) algorithm are used to build a prediction model. In this study, only 5 NASA \cite{NASA} datasets have been used and the accuracy was 80\% (using Area Under The Curve measurement). Also in \cite{rnn1} some classification techniques have been used to build a prediction model and one of the neural networks types (Random forest neural network) produces defect prediction models with better predictive ability than other learning techniques such as Naive Bayes and Decision Trees. The accuracy of the proposed model in this study was 90\% (using Area Under The Curve measurement) but this model is built to predict software faults in specific project for a software company (the model is trained using data gathered from the older version for the same project).
To build an efficient software fault prediction model we need a representative dataset from different domains. Several public datasets have been used by researchers in the past few years. Each dataset contains its software metrics and either a boolean variable to indicate the fault-proneness of a module or non-fault proneness or a number of faults found in that module. NASA \cite{NASA} datasets is commonly used as a benchmark dataset for building a software fault prediction model. NASA datasets contain methods-level metrics and class-level metrics. There are many researchers used NASA datasets \cite{pm3,pm4,oom3,oom6} and others. PROMISE datasets contain a collection of publicly available datasets (including NASA datasets) to help researchers to build predictive software models. PROMISE datasets contain 13 datasets from NASA datasets and many datasets from other resources. There are many researchers used PROMISE datasets \cite{tera2,tera3,tera4}. Lately, PROMISE datasets have been extended to TERA-PROMISE repository \cite{tera1}. TERA-PROMISE repository has 33 datasets contains CK metrics (class-level metrics) and 14 datasets contain McCabe and Halstead metrics (method-level metrics). Each dataset contains metrics for a specific project and also for older versions for this project (if exists).
\begin{figure*}

  \includegraphics[width=\textwidth ,height=4cm]{NN7}
%  \captionof{figure}{The DNN used to build a model to c}  
  \caption{Example of a Deep Neural Network}
  \label{DNN}
\end{figure*}

\subsection{Background on Deep Learning}
In recent years deep neural networks (DNN) have won many contests in machine learning and attracted widespread attention by achieving results and improvements better than many other machine learning methods. In the past decade, Deep neural networks achieve superhuman results in pattern recognition competitions \cite{DNNSuper}. The standard neural network consists of a number of layers each layer contains a number of neurons. The neurons in each layer are connected by the other neurons in the other layers by weighted connections and each neuron gets activated by either some environment variable or by the weighted connection with another neuron. each neuron produces some values through what we called "activation functions". The learning process is all about finding the weights that make the whole networks gives the desired behavior. This process also depends on the network structure and how neurons are connected to each other in each layer. To get the right weights this may require a heavy computational process, especially if the network structure is deep and complex. The neural network structure typically consists of an input layer and an output layer and some hidden layers (The network may have no hidden layers). The deep neural network is the network which consists of many hidden layers. The network shown in figure \ref{DNN} is an example of a deep neural network. The deep learning process is about finding the weights of the connections connects between each layer in the network.
Our DNN consists of an input layer, an output layer, and 5 hidden layers. The number of neurons in the input layer equals the number of the features in the target dataset. The second, third and forth hidden layers contain 30 neurons and the last and the first neuron contains only 10 neurons. The output layer contains only one neuron. We tried many configurations for our network and the mentioned configuration produces the best possible results for our model.
\\
We used relu \cite{relu} activation function to produce values from the input and hidden layers. The relu activation function is :
\[f(x) = max(0, x)\]
We depend on relu activation function as it's shown in \cite{relu1} that Relu activation function is faster and better than traditional tanh activation function especially in deep neural networks. Figure \ref{relu} shows Relu activation function
\begin{center}
\includegraphics[scale=0.5]{relu}
\captionof{figure}{Figure \label{relu}: RELU Activation Function \\ }
\end{center}

Also We used Sigmoid activation function in the output layer as it's shown in \cite{sigmoid} that sigmoid activation function works better in classification tasks if the classes are not linearly separable and its boundaries are more complex . The Sigmoid activation function is :
\[f(x) = \frac{1}{1 + e^{-x}}\] 
Figure \ref{sigmoid} shows Sigmoid activation function.
\begin{center}
\includegraphics[scale=0.5]{sigmoid}
\captionof{figure}{Figure \label{sigmoid}: RELU Activation Function \\ }
\end{center}

In addition we used adam optimizer \cite{adam} to update weights in the network and we used binary cross entropy function \cite{binaryCrossEntropy} to measure how off our predicted values are. 

\section{Experiment Setup}
In this section we will present our setup to compare our proposed model with other machine learning models
\subsection{Datasets}
There are many open source datasets available online used by researchers to create a defect prediction model. We selected 8 projects from NASA dataset and another 8 projects from TERA-PROMISE datasets as our dataset to build our proposed defect prediction model and compare it with other models. We selected projects with different size and a different set of metrics. Table \ref{Characteristic of Datasets Used In This Study} provides information for each project used in this study. It shows the dataset which contains this project (NASA or TERA-PROMISE), project name, number of metrics, number of entities (modules) in this project, number of defective entities (and the percentage of the defective entities) and number of non-defective entities (and the percentage of the non-defective entities)
\\
\subsection{Performance Measure}
\begin{table*}[h]
\caption{Characteristic of Datasets Used In This Study}
\label{Characteristic of Datasets Used In This Study}
\centering
\begin{tabular}{@{}cccccccc@{}}
\scriptsize Dataset & \scriptsize Project & \scriptsize Number of metrics & \scriptsize Number of entities & \scriptsize Number of defective entities & \scriptsize Number of non-defective entities \\
\midrule
\scriptsize NASA & \scriptsize CM1 & \scriptsize 37 & \scriptsize 327  & \scriptsize 42 (12.8\%)   & \scriptsize 285 (87.2\%)  \\
\scriptsize NASA & \scriptsize JM1 & \scriptsize 21 & \scriptsize 7782 & \scriptsize 1672 (21.5\%) & \scriptsize 6110 (78.5\%) \\
\scriptsize NASA & \scriptsize KC1 & \scriptsize 94 & \scriptsize 145  & \scriptsize 60   (41.4\%) & \scriptsize 85   (58.6\%) \\
\scriptsize NASA & \scriptsize KC2 & \scriptsize 21 & \scriptsize 522  & \scriptsize 107   (20.5\%) & \scriptsize 415   (79.5\%) \\
\scriptsize NASA & \scriptsize KC3 & \scriptsize 39 & \scriptsize 194  & \scriptsize 36   (18.6\%) & \scriptsize 158   (81.4\%) \\
\scriptsize NASA & \scriptsize PC1 & \scriptsize 37 & \scriptsize 705  & \scriptsize 61   (8.7\%) & \scriptsize 158   (91.3\%) \\
\scriptsize NASA & \scriptsize PC2 & \scriptsize 36 & \scriptsize 745  & \scriptsize 16   (2.1\%) & \scriptsize 158   (97.9\%) \\
\scriptsize NASA & \scriptsize MC2 & \scriptsize 39 & \scriptsize 126  & \scriptsize 44   (35.2\%) & \scriptsize 82   (65.1\%) \\
\scriptsize TERA-PROMISE & \scriptsize Ant v1.7 & \scriptsize 20 & \scriptsize 745  & \scriptsize 166   (22.3\%) & \scriptsize 579   (77.7\%) \\
\scriptsize TERA-PROMISE & \scriptsize JEdit v4.0 & \scriptsize 20 & \scriptsize 306  & \scriptsize 75   (24.5\%) & \scriptsize 231   (75.5\%) \\
\scriptsize TERA-PROMISE & \scriptsize Log4J v1.0 & \scriptsize 20 & \scriptsize 135  & \scriptsize 34   (25.2\%) & \scriptsize 101   (74.8\%) \\
\scriptsize TERA-PROMISE & \scriptsize Lucene v2.4 & \scriptsize 20 & \scriptsize 340  & \scriptsize 203   (59.7\%) & \scriptsize 137   (40.3\%) \\
\scriptsize TERA-PROMISE & \scriptsize POI v3.0 & \scriptsize 20 & \scriptsize 442  & \scriptsize 281   (63.6\%) & \scriptsize 161   (36.4\%) \\
\scriptsize TERA-PROMISE & \scriptsize Prop v1.0 & \scriptsize 20 & \scriptsize 18471  & \scriptsize 2738   (14.8\%) & \scriptsize 15733   (85.2\%) \\
\scriptsize TERA-PROMISE & \scriptsize Prop v5.0 & \scriptsize 20 & \scriptsize 8516  & \scriptsize 1299   (15.3\%) & \scriptsize 7217   (84.7\%) \\
\scriptsize TERA-PROMISE & \scriptsize XALAN v2.6 & \scriptsize 20 & \scriptsize 885  & \scriptsize 411   (46.4\%) & \scriptsize 474   (53.6\%) \\

\end{tabular}
\end{table*}
Many performance measures have been used in measuring a software defect prediction model. Measures like accuracy, recall (sensitivity), precision (specificity), F-measure and the Area under the receiver operating characteristic curve (AUC). Selecting a cut-off value and the skewness of defect data are major challenges in developing a software defect prediction model \cite{openIssues}. It's desirable to select a performance measure which is independent of the cut-off value and the skewness of defect data. Therefore we chose AUC to measure the performance of each model in this study (our proposed model and the other machine learning models including in the experiments). The AUC measure is computed by plotting a curve of the true negative rate (on the X-axis) against the true positive rate (on the Y-axis). The true positive rate is the proportion of the defective instances that are correctly classified. The true negative rate is the proportion of the non-defective instances that are correctly classified. The AUC values range between 0 and 1. Some studies \cite{tera3, AUCMeasure} suggest an AUC value between 0.9 to 1 indicates excellent performance. An AUC value between 0.8 to 0.9 indicates good performance. An AUC value between 0.7 to 0.8 indicates fair performance. An AUC value between 0.6 to 0.7 indicates poor performance. An AUC value less than 0.6 indicates a failed prediction.
\subsection{Deployment}
We have used Keras \cite{keras} API which is built on top of TensorFlow \cite{tensor} framework to build our proposed model. In addition, we have used scikit-learn (sklearn) \cite{sklearn} framework to build the other models which will be involved in the comparison.  Also, we developed a simple environment for our experiments to automate any experiment. So to compare between our model and the other learning models the essential step is to prepare the dataset by normalize the metrics data and binarize the defect label. Each dataset contains a defect label which either indicates whether the module is defect-prone or not or indicates the number of defects in the module. To binarize the defect label we set it to 0 if the module is not defective or contains zero defective and set it to 1 otherwise. Next step is to separate the data into two separate sets. One set for training a model and the other for testing (target) the trained model. To create the training and testing sets we have applied 5-fold cross validation (random splits). So each classifier is evaluated 5 times in each experiment with each project, each time we record the ROC AUC value. To get the final result of a specific experiment we get the average ROC AUC value (from the 5 recorded value).
\\
To deal with the randomness, we repeat the previous experiment (with random splits) 10 times and to get the performance of each classifier on each project we compute the average of the total 10 experiments.
\subsection{Results and Analysis}
The performance of the proposed model and the other learning models (mentioned earlier) are discussed in table \ref{Results of The Proposed Study}. The table present the ROC AUC value for each learning model and the value between the parentheses represents the standard deviation of the results. The best result in each project is presented in bold. From the results, we can conclude that our proposed model has the highest defect prediction AUC ROC value in 8 of 16 selected datasets. Also from the results, we can conclude that our approach has the highest average AUC ROC value followed by Random forest then Naive Bayes then decision trees. In addition, our  proposed model has the highest defect prediction AUC ROC values in the 3 biggest datasets (Prop v1.0, Prop 5.0 and JM1) 
\begin{table*}[h]
\caption{Results of The Proposed Study}
\label{Results of The Proposed Study}
\centering
\begin{tabular}{@{}ccccccc@{}}
\scriptsize Dataset & \scriptsize Project & \scriptsize DNN & \scriptsize RF & \scriptsize DT & \scriptsize NB \\
\midrule
\scriptsize NASA & \scriptsize CM1 & \scriptsize \textbf{0.871 (0.0012)}  & \scriptsize 0.855(0.0100)  & \scriptsize 0.804(0.0204)   & \scriptsize 0.809(0.0124) \\
\scriptsize NASA & \scriptsize JM1 & \scriptsize \textbf{0.790(0.0009)} & \scriptsize 0.783(0.0028) & \scriptsize 0.702 (0.0048)  & \scriptsize 0.782(0.0006) \\
\scriptsize NASA & \scriptsize KC1 & \scriptsize \textbf{0.751(0.0223)} & \scriptsize 0.720(0.0204)  & \scriptsize 0.678 (0.0339) & \scriptsize 0.693 (0.101) \\
\scriptsize NASA & \scriptsize KC2 & \scriptsize 0.835(0.0070) & \scriptsize 0.831 (0.0100)  & \scriptsize 0.790 (0.0146) & \scriptsize \textbf{0.836 (0.0032)} \\
\scriptsize NASA & \scriptsize KC3 & \scriptsize \textbf{0.810(0.0073)} & \scriptsize 0.803 (0.0100)  & \scriptsize 0.767 (0.0246) & \scriptsize 0.787 (0.0115) \\
\scriptsize NASA & \scriptsize PC1 & \scriptsize 0.915 (0.0048) & \scriptsize \textbf{0.920 (0.0042)} & \scriptsize 0.878 (0.0120) & \scriptsize 0.880 (0.0060) \\
\scriptsize NASA & \scriptsize PC2 & \scriptsize \textbf{0.979 (0.0000)} & \scriptsize 0.977 (0.0013) & \scriptsize 0.957 (0.0068) & \scriptsize 0.885 (0.0212) \\
\scriptsize NASA & \scriptsize MC2 & \scriptsize 0.697 (0.0247) & \scriptsize 0.690 (0.0237)  & \scriptsize 0.641 (0.0342) & \scriptsize \textbf{0.716 (0.0091)} \\
\scriptsize TERA-PROMISE & \scriptsize Ant v1.7 & \scriptsize \textbf{0.821 (0.0071)} & \scriptsize 0.813 (0.0068)  & \scriptsize 0.759 (0.0123) & \scriptsize 0.806 (0.0048) \\
\scriptsize TERA-PROMISE & \scriptsize JEdit v4.0 & \scriptsize 0.790 (0.0134) & \scriptsize \textbf{0.797 (0.0119)}  & \scriptsize 0.755 (0.0169) & \scriptsize 0.767 (0.0052) \\
\scriptsize TERA-PROMISE & \scriptsize Log4J v1.0 & \scriptsize 0.803 (0.0207) & \scriptsize 0.790 (0.0204)  & \scriptsize 0.739 (0.0274) & \scriptsize \textbf{0.835 (0.0120)} \\
\scriptsize TERA-PROMISE & \scriptsize Lucene v2.4 & \scriptsize 0.661 (0.0165) & \scriptsize \textbf{0.693 (0.0193)} & \scriptsize 0.655 (0.0208) & \scriptsize 0.593 (0.0091) \\
\scriptsize TERA-PROMISE & \scriptsize POI v3.0 & \scriptsize 0.775(0.0109) & \scriptsize \textbf{0.801(0.0133)}  & \scriptsize 0.775 (0.01694) & \scriptsize 0.540 (0.0093) \\
\scriptsize TERA-PROMISE & \scriptsize Prop v1.0 & \scriptsize \textbf{0.857 (0.0007)} & \scriptsize 0.843 (0.0017)  & \scriptsize 0.834 (0.0020) & \scriptsize 0.820 (0.0006) \\
\scriptsize TERA-PROMISE & \scriptsize Prop v5.0 & \scriptsize \textbf{0.850 (0.0007)} & \scriptsize 0.791 (0.0030)  & \scriptsize 0.789 (0.0024) & \scriptsize 0.829 (0.0019) \\
\scriptsize TERA-PROMISE & \scriptsize XALAN v2.6 & \scriptsize 0.738 (0.0072) & \scriptsize \textbf{0.759 (0.0094)}  & \scriptsize 0.716 (0.0102) & \scriptsize 0.711 (0.0036) \\
\midrule
& \scriptsize Average & \scriptsize \textbf{0.809} & \scriptsize 0.804  & \scriptsize 0.764 & \scriptsize 0.768
\end{tabular}
\end{table*}

\section{Threats to Validity}
In this section we describe the threats to validity for our study
\subsection{Threats to Internal Validity}
Internal validity concerns about the changes in independent variables in our experiments and whether we can conclude that these changes cause the observed results or not. In our experiment, we were concerned to check the change in the learning technique and re-run the experiment and observe the results. In all of our experiment, we fixed all independent variables except the learning technique. We were comparing our proposed learning technique against 3 other learning technique. In each experiment, we used the same dataset and the same cross-validation technique with each learning technique. So each experiment produces 4 results (ROC AUC values) each result related to each learning technique. Also, we run all experiments in the same environment.
\subsection{Threats to External Validity}
External validity concerns about the generalization of our results. We tried to select different types of datasets to test our experiment on it. We gathered our datasets from different projects in different datasets (NASA or PROMISE). Also, our criteria in project selection in NASA or PROMISE datasets was based on the ratio of defects, the number of tested modules in each project and the metrics used to describe each module in each project. So we selected projects with a high ratio of defects and a low ratio of defects. We selected projects with a high number of tested modules and a low number of tested modules. We selected projects with CK metrics and other with McCabe and Halstead metrics. We built our model to accept any number of metrics and adapt the deep neural network with this number of metrics. However, it would be better to test our model on more datasets with different characteristics and different metrics. This work is considered part of our future work. 
\section{Conclusion and Future Work}
Due to the un-feasibility of exhaustive testing, software defect prediction tools bring a new way to test the buggy modules first to minimize the number of defects in a software. Many machine learning techniques used to build a defect predictive model to tackle this challenge, however, there's still a need to improve the prediction accuracy of these models \cite{gen1}. In this study we built a defect predictive model using a deep neural network as it's shown in \cite{DNNSuper} that deep neural networks won many competitions in machine learning challenges. To evaluate our proposed model we compared our proposed model against other well-known machine learning models like the random forest, decision trees, and naive Bayes. We perform experiments on 16 projects from two available datasets (NASA) \cite{NASA} and PROMISE \cite{tera1}). To answer our research question which is how deep neural network classifier performs against the other classifiers in predicting faulty modules. We can conclude that our classifier performs better than the other machine learning techniques in most of the datasets. In the future, We intend to use more advanced deep learning techniques and explore more datasets from different resources. In addition, it would be promising to try some of feature generation techniques to generate the features which will help in improving the model's accuracy. We see that some researchers took the first step in this goal \cite{CNN} so we will consider their work in our future work.
\begin{thebibliography}{50}

\bibitem{survey}
World Quality Report 2016-17 | Capgemini Worldwide (retrieved 16.09.16) https://www.capgemini.com/thought-leadership/world-quality-report-2016-17

\bibitem{mccabe}
McCabe, T.J., 1976. A complexity measure. IEEE Transactions on software Engineering, (4), pp.308-320.
Vancouver	


\bibitem{hals}
Halstead, M.H., 1977. Elements of software science.

\bibitem{gen1}
Malhotra, R., 2015. A systematic review of machine learning techniques for software fault prediction. Applied Soft Computing, 27, pp.504-518.

\bibitem{pm1}
Guo, L., Cukic, B. and Singh, H., 2003, October. Predicting fault prone modules by the dempster-shafer belief networks. In Automated Software Engineering, 2003. Proceedings. 18th IEEE International Conference on (pp. 249-252). IEEE.

\bibitem{pm2}
Singh, Y., Kaur, A. and Malhotra, R., 2010. Prediction of fault-prone software modules using statistical and machine learning methods. International Journal of Computer Applications, 1(22), pp.8-15.

\bibitem{pm3}
Dejaeger, K., Verbraken, T. and Baesens, B., 2013. Toward comprehensible software fault prediction models using bayesian network classifiers. IEEE Transactions on Software Engineering, 39(2), pp.237-257.

\bibitem{pm4}
Cahill, J., Hogan, J.M. and Thomas, R., 2013, June. Predicting fault-prone software modules with rank sum classification. In Software Engineering Conference (ASWEC), 2013 22nd Australian (pp. 211-219). IEEE.

\bibitem{pm5}
Arar, Ö.F. and Ayan, K., 2015. Software defect prediction using cost-sensitive neural network. Applied Soft Computing, 33, pp.263-277.

\bibitem{pm6}
Catal, C. and Diri, B., 2009. Investigating the effect of dataset size, metrics sets, and feature selection techniques on software fault prediction problem. Information Sciences, 179(8), pp.1040-1058.

\bibitem{oom1}
Chidamber, S.R. and Kemerer, C.F., 1994. A metrics suite for object oriented design. IEEE Transactions on software engineering, 20(6), pp.476-493.

\bibitem{oom2}
Virtual Machinery - Sidebar 3 - WMC, CBO, RFC, LCOM, DIT, NOC - 'The Chidamber and Kemerer Metrics' (retrieved 01.01.18) http://www.virtualmachinery.com/sidebar3.htm

\bibitem{NASA}
tera-PROMISE Category: McCabe and Halsted (retrieved 01.01.18) http://openscience.us/repo/defect/mccabehalsted/

\bibitem{tera1}
tera-PROMISE: The largest repositories of SE research data (retrieved 01.01.18) http://openscience.us/repo/index.html

\bibitem{oom3}
Okutan, A. and Yıldız, O.T., 2014. Software defect prediction using Bayesian networks. Empirical Software Engineering, 19(1), pp.154-181.‏

\bibitem{oom4}
He, P., Li, B., Liu, X., Chen, J. and Ma, Y., 2015. An empirical study on software defect prediction with a simplified metric set. Information and Software Technology, 59, pp.170-190.

\bibitem{oom5}
Murillo-Morera, J., Castro-Herrera, C., Arroyo, J. and Fuentes-Fernández, R., 2016. An automated defect prediction framework using genetic algorithms: A validation of empirical studies. Inteligencia Artificial, 19(57), pp.114-137.

\bibitem{oom6}
Jabangwe, R., Börstler, J., Šmite, D. and Wohlin, C., 2015. Empirical evidence on the link between object-oriented measures and external quality attributes: a systematic literature review. Empirical Software Engineering, 20(3), pp.640-693.

\bibitem{tera2}
das Dôres, S.N., Alves, L., Ruiz, D.D. and Barros, R.C., 2016, April. A meta-learning framework for algorithm recommendation in software fault prediction. In Proceedings of the 31st Annual ACM Symposium on Applied Computing (pp. 1486-1491). ACM.

\bibitem{tera3}
Zhang, F., Zheng, Q., Zou, Y. and Hassan, A.E., 2016, May. Cross-project defect prediction using a connectivity-based unsupervised classifier. In Proceedings of the 38th International Conference on Software Engineering (pp. 309-320). ACM.

\bibitem{tera4}
Hussain, S., 2016, April. Threshold analysis of design metrics to detect design flaws: student research abstract. In Proceedings of the 31st Annual ACM Symposium on Applied Computing (pp. 1584-1585). ACM.


\bibitem{dt1}
Sawadpong, P. and Allen, E.B., 2016, January. Software Defect Prediction Using Exception Handling Call Graphs: A Case Study. In High Assurance Systems Engineering (HASE), 2016 IEEE 17th International Symposium on (pp. 55-62). IEEE.

\bibitem{rnn1}
Koroglu, Y., Sen, A., Kutluay, D., Bayraktar, A., Tosun, Y., Cinar, M. and Kaya, H., 2016, May. Defect prediction on a legacy industrial software: A case study on software with few defects. In Conducting Empirical Studies in Industry (CESI), 2016 IEEE/ACM 4th International Workshop on (pp. 14-20). IEEE.

\bibitem{dnn1}
Schmidhuber, J., 2015. Deep learning in neural networks: An overview. Neural networks, 61, pp.85-117.

\bibitem{minist}
MNIST handwritten digit database, Yann LeCun, Corinna Cortes and Chris Burges (retrieved 01.01.18) http://yann.lecun.com/exdb/mnist/

\bibitem{ckjm}
ckjm - A Tool for Calculating Chidamber and Kemerer Java Metrics (retrieved 01.01.18) http://www.spinellis.gr/sw/ckjm/doc/indexw.html

\bibitem{relu}
Nair, V. and Hinton, G.E., 2010. Rectified linear units improve restricted boltzmann machines. In Proceedings of the 27th international conference on machine learning (ICML-10) (pp. 807-814).

\bibitem{relu1}
Krizhevsky, A., Sutskever, I. and Hinton, G.E., 2012. Imagenet classification with deep convolutional neural networks. In Advances in neural information processing systems (pp. 1097-1105).

\bibitem{DNNSuper}
Schmidhuber, J., 2015. Deep learning in neural networks: An overview. Neural networks, 61, pp.85-117.

\bibitem{sigmoid}
Acharya, U.R., Bhat, P.S., Iyengar, S.S., Rao, A. and Dua, S., 2003. Classification of heart rate data using artificial neural network and fuzzy equivalence relation. Pattern recognition, 36(1), pp.61-68.

\bibitem{adam}
Kingma, D.P. and Ba, J., 2014. Adam: A method for stochastic optimization. arXiv preprint arXiv:1412.6980.‏

\bibitem{binaryCrossEntropy}
Drozdzal, M., Vorontsov, E., Chartrand, G., Kadoury, S. and Pal, C., 2016. The importance of skip connections in biomedical image segmentation. In Deep Learning and Data Labeling for Medical Applications (pp. 179-187). Springer, Cham.

\bibitem{openIssues}
Arora, I., Tetarwal, V. and Saha, A., 2015. Open issues in software defect prediction. Procedia Computer Science, 46, pp.906-912.

\bibitem{AUCMeasure}
Gorunescu, F., 2011. Data Mining:Concepts, models and techniques (Vol. 12). Springer Science \& Business Media.

\bibitem{keras}
Keras Documentation (retrieved 01.01.18) https://keras.io/

\bibitem{tensor}
TensorFlow (retrieved 01.01.18) https://www.tensorflow.org/

\bibitem{sklearn}
scikit-learn: machine learning in Python - scikit-learn 0.19.1 documentation (retrieved 01.01.18) http://scikit-learn.org/stable/

\bibitem{CNN}
Li, J., He, P., Zhu, J. and Lyu, M.R., 2017, July. Software defect prediction via convolutional neural network. In Software Quality, Reliability and Security (QRS), 2017 IEEE International Conference on (pp. 318-328). IEEE.

\end{thebibliography}



\end{document}
